#!/usr/bin/env sh
set -euo pipefail

# max length for the prints
maxlen_artist=50
maxlen_title=50

exit(0)

# get the current player
#for p in $(playerctl -l); do
#    if [[ $(playerctl --player=${p} status 2>/dev/null) == "Playing" ]]; then
#        player=$p
#        break
#    fi
#done

## exit if no player is running
#if [[ -z ${player+x} ]]; then
#    echo "{}"
#    exit
#fi
player=spotify

# get some info
metadata=$(playerctl metadata --player=${player} --format '{{artist}}\{{title}}\{{album}}' | tr \" \')

IFS='\' read -r -a array <<< "$metadata"
artist="${array[0]}"
title="${array[1]}"
album="${array[2]}"

# cut artist and title info to the right size
if [ ${#artist} -gt ${maxlen_artist} ]; then
    artist=$(echo $artist | cut -c1-${maxlen_artist})"..."
fi
if [ ${#title} -gt ${maxlen_title} ]; then
    title=$(echo $title | cut -c1-${maxlen_title})"..."
fi

# choose the right icon
case "${player%\.*}" in
    mpd)
        icon=""
        tooltip="Album: "$album
        ;;
    spotify)
        icon=""
        tooltip="Album: "$album
        ;;
    spotifyd)
        icon=""
        tooltip="Album: "$album
        ;;
    chromium)
        icon=""
        tooltip="Browser"
        ;;
    *)
        icon=">"
        ;;
esac

# and finally return a json string
text="$icon  $artist - $title"
echo -e "{\"text\":\""$text"\", \"tooltip\":\"$tooltip\"}"
