#!/bin/sh

VPN=$(connmanctl services | grep "^*[A ][RO].*vpn" | sed 's/ \+/ /' | cut -d "_" -f1 | cut -d " " -f2-)

if [[ ! -z $VPN ]]; then
    icon=''
    class="connected"
else
    icon=''
    class="disconnected"
fi

echo -e "{\"text\":\""$icon"\", \"tooltip\":\" Connected to "$VPN"\", \"class\":\""$class"\"}"
