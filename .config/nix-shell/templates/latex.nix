{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    (texlive.combine {
      inherit (texlive)
      scheme-medium
      csquotes
      carlito
      fontaxes
      passivetex;
    })
    biber
  ];
}
