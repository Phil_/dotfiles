{ pkgs ? import <nixpkgs> {} }:

let
  mach-nix = import (builtins.fetchGit {
    url = "https://github.com/DavHau/mach-nix/";
    ref = "2.4.1";
  });
  customPython = mach-nix.mkPython {
    python = mach-nix.nixpkgs.python38;
    disable_checks = true;

    requirements = ''
      matplotlib
      numpy

      pynvim
      python-language-server
      jedi
      snowballstemmer
      pluggy
    '';
  };

in pkgs.mkShell {
  buildInputs = with pkgs; [
    customPython
  ];
}
