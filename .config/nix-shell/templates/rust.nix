{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    rust-analyzer-unstable
    cargo
    rustc
  ];
}
