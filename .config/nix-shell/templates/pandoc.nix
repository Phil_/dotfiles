{ 
  pkgs ? import <nixpkgs> {}
}:
pkgs.mkShell {
  name = "gropro-shell";
  buildInputs = with pkgs; [
    # base
    python38

    # python packages
    python38Packages.numpy
    python38Packages.matplotlib
    python38Packages.sphinx

    # for vim
    python38Packages.pynvim
    python38Packages.pylint
    python38Packages.jedi
    python38Packages.snowballstemmer

    pandoc
    haskellPackages.pandoc-citeproc
  ];
}
