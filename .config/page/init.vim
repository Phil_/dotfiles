call plug#begin('~/.config/nvim/plugged')
" kb layout
Plug 'nicwest/vim-workman'
" Fancy stuffs
Plug 'edkolev/tmuxline.vim'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" colorscheme
Plug 'ayu-theme/ayu-vim'
Plug 'joshdick/onedark.vim'

call plug#end()
filetype plugin indent on

set termguicolors
let ayucolor="mirage"
try
    colorscheme ayu
catch /^Vim\%((\a\+)\)\=:E185/
    colorscheme koehler
endtry



" ---------------------------
"           sets
" ---------------------------
" set guifont=Hack:h10:cDEFAULT

set mouse=a
set cmdheight=2
set ignorecase
set smartcase
set incsearch
set hlsearch

" powerline / tmuxline
let g:airline_powerline_fonts = 1
let g:airline#extensions#whitespace#enabled = 0
let g:tmux_navigator_no_mappings = 1
" Update changed buffer when switching to Tmux
let g:tmux_navigator_save_on_switch = 1
let g:tmuxline_preset = 'vim_powerline_1'
let g:tmuxline_preset = {
      \'a'    : '#S',
      \'win'  : ['#I', '#W'],
      \'cwin' : ['#I', '#W'],
      \'x'    : '#(whoami)@#H',
      \'z'    : '%a %R'}

" workman layout setting
let g:workman_normal_qwerty = 1

" clear after search
nnoremap <Leader><Leader> :noh<CR>

" search in visual
vnoremap / y/<C-R>"<CR>
