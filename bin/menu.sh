#!/bin/bash
export SWAYSOCK=/run/user/$(id -u)/sway-ipc.$(id -u).$(pgrep -x sway).sock
compgen -c | sort -u | fzf --print-query --reverse --border --color=16 | tail -n1 | xargs -r swaymsg -t command exec
